(function(e, t) {
    "object" == typeof exports ? module.exports = t(require("wbxml"), require("activesync/codepages")) : "function" == typeof define && define.amd ? define([ "wbxml", "activesync/codepages" ], t) : e.ActiveSyncProtocol = t(WBXML, ActiveSyncCodepages);
})(this, function(e, t) {
    function n() {}
    function o(e, t, n) {
        function o() {
            var e = this instanceof o ? this : Object.create(o.prototype), t = Error(), r = 1;
            if (e.stack = t.stack.substring(t.stack.indexOf("\n") + 1), e.message = arguments[0] || t.message, 
            n) {
                r += n.length;
                for (var s = 0; s < n.length; s++) e[n[s]] = arguments[s + 1];
            }
            var i = /@(.+):(.+)/.exec(e.stack);
            return e.fileName = arguments[r] || i && i[1] || "", e.lineNumber = arguments[r + 1] || i && i[2] || 0, 
            e;
        }
        return o.prototype = Object.create((t || Error).prototype), o.prototype.name = e, 
        o.prototype.constructor = o, o;
    }
    function r(e) {
        var t = "http://schemas.microsoft.com/exchange/autodiscover/", n = {
            rq: t + "mobilesync/requestschema/2006",
            ad: t + "responseschema/2006",
            ms: t + "mobilesync/responseschema/2006"
        };
        return n[e] || null;
    }
    function s(e) {
        var t = e.split(".").map(function(e) {
            return parseInt(e);
        });
        this.major = t[0], this.minor = t[1];
    }
    function i(e, t, n) {
        var o = "Basic " + btoa(t + ":" + n);
        e.setRequestHeader("Authorization", o);
    }
    function a(e, t, o, r, s) {
        r || (r = n);
        var i = e.substring(e.indexOf("@") + 1);
        c("autodiscover." + i, e, t, o, s, function(n, a) {
            n instanceof f || n instanceof m ? c(i, e, t, o, s, r) : r(n, a);
        });
    }
    function c(e, t, n, o, r, s) {
        var i = "https://" + e + "/autodiscover/autodiscover.xml";
        return d(i, t, n, o, r, s);
    }
    function d(e, t, n, o, s, c) {
        var d = new XMLHttpRequest({
            mozSystem: !0,
            mozAnon: !0
        });
        d.open("POST", e, !0), i(d, t, n), d.setRequestHeader("Content-Type", "text/xml"), 
        d.setRequestHeader("User-Agent", h), d.timeout = o, d.upload.onprogress = d.upload.onload = function() {
            d.timeout = 0;
        }, d.onload = function() {
            if (d.status < 200 || d.status >= 300) return c(new m(d.statusText, d.status));
            var e = Math.random();
            self.postMessage({
                uid: e,
                type: "configparser",
                cmd: "accountactivesync",
                args: [ d.responseText, s ]
            }), self.addEventListener("message", function t(r) {
                var s = r.data;
                if ("configparser" == s.type && "accountactivesync" == s.cmd && s.uid == e) {
                    self.removeEventListener(r.type, t);
                    var i = s.args, d = i[0], u = i[1], l = i[2];
                    u ? c(new f(u), d) : l ? a(l, n, o, c, !0) : c(null, d);
                }
            });
        }, d.ontimeout = d.onerror = function() {
            c(new m("Error getting Autodiscover URL", null));
        };
        var u = '<?xml version="1.0" encoding="utf-8"?>\n<Autodiscover xmlns="' + r("rq") + '">\n' + "  <Request>\n" + "    <EMailAddress>" + t + "</EMailAddress>\n" + "    <AcceptableResponseSchema>" + r("ms") + "</AcceptableResponseSchema>\n" + "  </Request>\n" + "</Autodiscover>";
        d.send(u);
    }
    function u(e, t, n) {
        this._deviceId = e || "v140Device", this._deviceType = n || "KaiOS", this.timeout = 0, 
        this._connected = !1, this._waitingForConnection = !1, this._connectionError = null, 
        this._connectionCallbacks = [], this.baseUrl = null, this._username = null, this._password = null, 
        this.versions = [], this.supportedCommands = [], this.currentVersion = null, this.policyKey = t || 0, 
        this.onmessage = null;
    }
    var l = {}, h = "KaiOS ActiveSync Client", p = o("ActiveSync.AutodiscoverError");
    l.AutodiscoverError = p;
    var f = o("ActiveSync.AutodiscoverDomainError", p);
    l.AutodiscoverDomainError = f;
    var m = o("ActiveSync.HttpError", null, [ "status" ]);
    return l.HttpError = m, l.Version = s, s.prototype = {
        eq: function(e) {
            return e instanceof s || (e = new s(e)), this.major === e.major && this.minor === e.minor;
        },
        ne: function(e) {
            return !this.eq(e);
        },
        gt: function(e) {
            return e instanceof s || (e = new s(e)), this.major > e.major || this.major === e.major && this.minor > e.minor;
        },
        gte: function(e) {
            return e instanceof s || (e = new s(e)), this.major >= e.major || this.major === e.major && this.minor >= e.minor;
        },
        lt: function(e) {
            return !this.gte(e);
        },
        lte: function(e) {
            return !this.gt(e);
        },
        toString: function() {
            return this.major + "." + this.minor;
        }
    }, l.autodiscover = a, l.raw_autodiscover = d, l.Connection = u, u.prototype = {
        _notifyConnected: function(e) {
            e && this.disconnect();
            for (var t in Iterator(this._connectionCallbacks)) {
                var n = t[1];
                n.apply(n, arguments);
            }
            this._connectionCallbacks = [];
        },
        get connected() {
            return this._connected;
        },
        open: function(e, t, n) {
            var o = "/Microsoft-Server-ActiveSync";
            this.baseUrl = e, this.baseUrl.endsWith(o) || (this.baseUrl += o), this._username = t, 
            this._password = n;
        },
        connect: function(n) {
            return this.connected ? (n && n(null), void 0) : (n && this._connectionCallbacks.push(n), 
            this._waitingForConnection || (this._waitingForConnection = !0, this._connectionError = null, 
            this.getOptions(function(n, o) {
                return this._waitingForConnection = !1, this._connectionError = n, n ? (console.error("Error connecting to ActiveSync:", n), 
                this._notifyConnected(n, o)) : (this._connected = !0, this.versions = o.versions, 
                this.supportedCommands = o.commands, this.currentVersion = new s(o.versions.slice(-1)[0]), 
                this.enforcement(function(n, r) {
                    var s = "Error get Security Policy for ActiveSync: ", i = !1;
                    if (n) {
                        if (console.error(s, n), 449 !== n.status) return this._notifyConnected(n, o);
                        i = !0;
                    }
                    var a = new e.EventParser(), c = t.FolderHierarchy.Tags, d = t.Common.Enums, u = t.FolderHierarchy.Enums;
                    a.addEventListener([ c.FolderSync, c.Status ], function(e) {
                        var t = e.children[0].textContent;
                        if (t === u.Status.Success) return this._notifyConnected(null, o);
                        if (t === d.Status.DeviceNotProvisioned || t === d.Status.PolicyRefresh || t === d.Status.InvalidPolicyKey || i) this.getPolicyKey(!1, function(e) {
                            return e ? (console.error(s, e), this._notifyConnected(e, o)) : (this.getPolicyKey(!0, function(e) {
                                return e ? (console.error(s, e), this._notifyConnected(e, o)) : this._notifyConnected(null, o);
                            }.bind(this)), void 0);
                        }.bind(this)); else {
                            if (t !== d.Status.RemoteWipeRequested) return n = "enforcement error, status: " + t, 
                            console.error(s, n), this._notifyConnected(n, o);
                            this.deviceWipe(function(e) {
                                return e ? (console.error(s, e), this._notifyConnected(e, o)) : this._notifyConnected(null, o);
                            }.bind(this));
                        }
                    }.bind(this));
                    try {
                        a.run(r);
                    } catch (l) {
                        console.error(s, l);
                    }
                }.bind(this)), void 0);
            }.bind(this))), void 0);
        },
        disconnect: function() {
            if (this._waitingForConnection) throw new Error("Can't disconnect while waiting for server response");
            this._connected = !1, this.versions = [], this.supportedCommands = [], this.currentVersion = null;
        },
        getDeviceInfo: function(e) {
            var t = Math.random();
            self.postMessage({
                uid: t,
                type: "deviceInfo",
                cmd: "get"
            }), self.addEventListener("message", function n(o) {
                var r = o.data;
                if ("deviceInfo" == r.type && "get" == r.cmd && r.uid == t) {
                    self.removeEventListener(o.type, n);
                    var s = r.args, r = s[0], i = s[1];
                    i ? e(null, i) : e(r);
                }
            });
        },
        provision: function(n, o) {
            var r = t.Provision.Tags, s = t.Settings.Tags, i = new e.Writer("1.3", 1, "UTF-8");
            i.stag(r.Provision);
            var a = !n && this.currentVersion.gte("12.0");
            a ? this.getDeviceInfo(function(e, t) {
                t && o(t), i.stag(s.DeviceInformation).stag(s.Set).tag(s.FriendlyName, e.deviceName).tag(s.Model, e.modelName).tag(s.IMEI, e.imei).tag(s.OS, e.OSName).tag(s.OSLanguage, e.OSLanguage), 
                e.phoneNumber.length > 0 && i.tag(s.PhoneNumber, e.phoneNumber), this.currentVersion.gte("14.0") && i.tag(s.MobileOperator, e.operatorName), 
                i.etag().etag(), i.stag(r.Policies).stag(r.Policy).tag(r.PolicyType, "MS-EAS-Provisioning-WBXML"), 
                i.etag().etag().etag(), this.postCommand(i, o);
            }.bind(this)) : (i.stag(r.Policies).stag(r.Policy).tag(r.PolicyType, "MS-EAS-Provisioning-WBXML"), 
            n && i.tag(r.PolicyKey, this.policyKey).tag(r.Status, 1), i.etag().etag().etag(), 
            this.postCommand(i, o));
        },
        enforcement: function(n) {
            var o = t.FolderHierarchy.Tags, r = new e.Writer("1.3", 1, "UTF-8");
            r.stag(o.FolderSync).tag(o.SyncKey, "0").etag(), this.postCommand(r, n);
        },
        getPolicyKey: function(n, o) {
            this.provision(n, function(r, s) {
                r && o(r);
                var i = new e.EventParser(), a = t.Provision.Tags, c = [ a.Provision, a.Policies, a.Policy ];
                i.addEventListener(c.concat(a.PolicyKey), function(e) {
                    this.policyKey = e.children[0].textContent, o(null);
                }.bind(this)), n || i.addEventListener(c.concat([ a.Data, a.EASProvisionDoc ]), function(e) {
                    this.securitySettings(a, e);
                }.bind(this));
                try {
                    i.run(s);
                } catch (d) {
                    o(d);
                }
            }.bind(this));
        },
        securitySettings: function(e, t) {
            for (var n in Iterator(t.children)) {
                var o = n[1];
                switch (o.children.length ? o.children[0].textContent : null, o.tag) {
                  case e.DevicePasswordEnabled:
                    break;

                  case e.AlphanumericDevicePasswordRequired:
                    break;

                  case e.PasswordRecoveryEnabled:
                    break;

                  case e.RequireStorageCardEncryption:
                    break;

                  case e.AttachmentsEnabled:
                    break;

                  case e.MinDevicePasswordLength:
                    break;

                  case e.MaxInactivityTimeDeviceLock:
                    break;

                  case e.MaxAttachmentSize:
                    break;

                  case e.MaxDevicePasswordFailedAttempts:
                    break;

                  case e.AllowSimpleDevicePassword:
                    break;

                  case e.DevicePasswordExpiration:
                    break;

                  case e.DevicePasswordHistory:                }
            }
        },
        wipeCommand: function(t, n, o) {
            var r = new e.Writer("1.3", 1, "UTF-8");
            r.stag(t.Provision), n && r.stag(t.RemoteWipe).tag(t.Status, 1).etag(), r.etag(), 
            this.postCommand(r, function(n, r) {
                var s = new e.EventParser();
                n && o(n), s.addEventListener([ t.Provision, t.Status ], function(e) {
                    var t = e.children[0].textContent;
                    if ("1" === t) o(null); else {
                        var n = "Remote wipe error, error status: " + t;
                        console.error(n), o(n);
                    }
                });
                try {
                    s.run(r);
                } catch (i) {
                    o(i);
                }
            });
        },
        deviceWipe: function(e) {
            var n = t.Provision.Tags;
            this.wipeCommand(n, !1, function(t) {
                return t ? (e(t), void 0) : (this.wipeCommand(n, !0, function(t) {
                    if (t) return e(t), void 0;
                    e(null);
                    var n = Math.random();
                    self.postMessage({
                        uid: n,
                        type: "deviceInfo",
                        cmd: "reset"
                    });
                }), void 0);
            }.bind(this));
        },
        getOptions: function(e) {
            e || (e = n);
            var t = this, o = new XMLHttpRequest({
                mozSystem: !0,
                mozAnon: !0
            });
            o.open("OPTIONS", this.baseUrl, !0), i(o, this._username, this._password), o.setRequestHeader("User-Agent", h), 
            o.timeout = this.timeout, o.upload.onprogress = o.upload.onload = function() {
                o.timeout = 0;
            }, o.onload = function() {
                if (o.status < 200 || o.status >= 300) return console.error("ActiveSync options request failed with response " + o.status), 
                t.onmessage && t.onmessage("options", "error", o, null, null, null, null), e(new m(o.statusText, o.status)), 
                void 0;
                var n = {
                    versions: o.getResponseHeader("MS-ASProtocolVersions").split(/\s*,\s*/),
                    commands: o.getResponseHeader("MS-ASProtocolCommands").split(/\s*,\s*/)
                };
                t.onmessage && t.onmessage("options", "ok", o, null, null, null, n), e(null, n);
            }, o.ontimeout = o.onerror = function() {
                var n = new Error("Error getting OPTIONS URL");
                console.error(n), t.onmessage && t.onmessage("options", "timeout", o, null, null, null, null), 
                e(n);
            }, o.responseType = "text", o.send();
        },
        supportsCommand: function(e) {
            if (!this.connected) throw new Error("Connection required to get command");
            return "number" == typeof e && (e = t.__tagnames__[e]), -1 !== this.supportedCommands.indexOf(e);
        },
        doCommand: function() {
            console.warn("doCommand is deprecated. Use postCommand instead."), this.postCommand.apply(this, arguments);
        },
        postCommand: function(e, n, o, r, s) {
            var i = "application/vnd.ms-sync.wbxml";
            if ("string" == typeof e || "number" == typeof e) this.postData(e, i, null, n, o, r); else {
                var a = t.__tagnames__[e.rootTag];
                this.postData(a, i, "blob" === e.dataType ? e.blob : e.buffer, n, o, r, s);
            }
        },
        postData: function(n, o, r, s, a, c, d) {
            if ("number" == typeof n && (n = t.__tagnames__[n]), !this.supportsCommand(n)) {
                var u = new Error("This server doesn't support the command " + n);
                return console.error(u), s(u), void 0;
            }
            var l = [ [ "Cmd", n ], [ "User", this._username ], [ "DeviceId", this._deviceId ], [ "DeviceType", this._deviceType ] ];
            if (a) {
                for (var p in Iterator(l)) {
                    var f = p[1];
                    if (f[0] in a) throw new TypeError("reserved URL parameter found");
                }
                for (var g in Iterator(a)) l.push(g);
            }
            var y = l.map(function(e) {
                return encodeURIComponent(e[0]) + "=" + encodeURIComponent(e[1]);
            }).join("&"), v = new XMLHttpRequest({
                mozSystem: !0,
                mozAnon: !0
            });
            if (v.open("POST", this.baseUrl + "?" + y, !0), i(v, this._username, this._password), 
            v.setRequestHeader("MS-ASProtocolVersion", this.currentVersion), v.setRequestHeader("Content-Type", o), 
            v.setRequestHeader("User-Agent", h), v.setRequestHeader("X-MS-PolicyKey", this.policyKey), 
            c) for (var p in Iterator(c)) {
                var _ = p[0], b = p[1];
                v.setRequestHeader(_, b);
            }
            v.timeout = this.timeout, v.upload.onprogress = v.upload.onload = function() {
                v.timeout = 0;
            }, v.onprogress = function(e) {
                d && d(e.loaded, e.total);
            };
            var S = this, T = arguments;
            v.onload = function() {
                if (451 === v.status) return S.baseUrl = v.getResponseHeader("X-MS-Location"), S.onmessage && S.onmessage(n, "redirect", v, l, c, r, null), 
                S.postData.apply(S, T), void 0;
                if (v.status < 200 || v.status >= 300) return console.error("ActiveSync command " + n + " failed with " + "response " + v.status), 
                S.onmessage && S.onmessage(n, "error", v, l, c, r, null), s(new m(v.statusText, v.status)), 
                void 0;
                var o = null;
                v.response.byteLength > 0 && (o = new e.Reader(new Uint8Array(v.response), t)), 
                S.onmessage && S.onmessage(n, "ok", v, l, c, r, o), s(null, o);
            }, v.ontimeout = v.onerror = function(e) {
                var t = new Error("Command URL " + e.type + " for command " + n + " at baseUrl " + this.baseUrl);
                console.error(t), S.onmessage && S.onmessage(n, e.type, v, l, c, r, null), s(t);
            }.bind(this), v.responseType = "arraybuffer", v.send(r);
        }
    }, l;
});