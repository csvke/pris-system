
(function(exports){'use strict';function ah_getCompatible(list,mcc,mnc,type){var apns=list[mcc]?(list[mcc][mnc]||[]):[];Object.keys(apns).forEach((key)=>{if(apns[key]['protocol']===undefined){console.log('apn_helper.js,ah_getCompatible no protocol');apns[key]['protocol']='notDefined';apns[key]['roaming_protocol']='notDefined';}else{console.log('apn_helper.js,ah_getCompatible there is protocol');}});return apns;}
function ah_getAll(list,mcc,mnc){var apns=list[mcc]?(list[mcc][mnc]||[]):[];Object.keys(apns).forEach((key1)=>{if(apns[key1]['protocol']===undefined){console.log('apn_helper.js,ah_getAll no protocol');apns[key1]['protocol']='notDefined';apns[key1]['roaming_protocol']='notDefined';}else{console.log('apn_helper.js,ah_getAll there is protocol');}});return apns;}
var ApnHelper={getCompatible:ah_getCompatible,getAll:ah_getAll};exports.ApnHelper=ApnHelper;})(this);