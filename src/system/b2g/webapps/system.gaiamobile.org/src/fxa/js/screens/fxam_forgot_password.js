﻿/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- /
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

/* global FxaModuleStates, FxaModuleUI, FxaModule, FxaModuleNavigation,
   FxModuleServerRequest, FxaModuleOverlay, FxaModuleManager, EntrySheet,
   BrowserFrame */
/* exported FxaModuleSigning */

'use strict';

/**
 * This module checks the validity of an email address, and if valid,
 * determines which screen to go next.
 */

var FxaModuleForgotPassword = (function () {
  var self = this;
  function _checkEmail(email) {
    var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
    return reg.test(email);
  }
  var Module = Object.create(FxaModule);
  Module.init = function init(options) {

    this.importElements(
      'fxa-reset-email-input',
      'fxa-reset-your-password'
    );

    var placeholder = navigator.mozL10n.get('fxa-placeholder');
    this.fxaResetEmailInput.setAttribute('placeholder', placeholder);

    ViewManager.setSkMenu();
    if (this.initialized) {
      return;
    }

    FxModuleServerRequest.requestPasswordReset('', function onServerResponse(response) {
      NavigationMap.currentActivatedLength = 0;
      FxaModuleManager.setParam('email', email);
      gotoNextStepCallback(FxaModuleStates.RESET_PASSWORD_SUCCESS);
    }.bind(this),
    function onError(response) {
      self.showErrorResponse(response);
    }.bind(this));
    this.initialized = true;
  };
  Module.onNext = function onNext(gotoNextStepCallback) {
    var self = this;
    var email = this.fxaResetEmailInput.value;
    if (!_checkEmail(email)) {
      self.showErrorResponse({ error: 'INVALID_EMAIL' });
      return;
    }
    //  FxaModuleOverlay.show('fxa-connecting');
    FxModuleServerRequest.requestPasswordReset(
    email,
    function onServerResponse(response) {
      NavigationMap.currentActivatedLength = 0;
      //FxaModuleOverlay.hide();
      FxaModuleManager.setParam('email', email);
      gotoNextStepCallback(FxaModuleStates.RESET_PASSWORD_SUCCESS);
    }.bind(this),
    function onError(response) {
      NavigationMap.currentActivatedLength = 0;
      //FxaModuleOverlay.hide();
      self.showErrorResponse(response);
    }.bind(this)
   );
  };
  Module.onBack = function onBack() {
    FxaModuleUI.enableNextButton();
  };
  return Module;
}());
