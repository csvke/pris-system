
var musicdb;var firstScanDone=false;var reparsingMetadata=false;function initDB(){musicdb=new MediaDB('music',metadataParserWrapper,{indexes:['metadata.album','metadata.artist','metadata.title','metadata.rated','metadata.played','date'],batchSize:1,autoscan:false,updateRecord:updateRecord,reparsedRecord:reparsedRecord,version:3});function metadataParserWrapper(file,onsuccess,onerror){LazyLoader.load('js/metadata_scripts.js',function(){AudioMetadata.parse(file).then(onsuccess,onerror);});}
function updateRecord(record,oldVersion,newVersion){if(oldVersion===2){record.needsReparse=true;reparsingMetadata=true;}
return record.metadata;}
function reparsedRecord(oldMetadata,newMetadata){newMetadata.rated=oldMetadata.rated;newMetadata.played=oldMetadata.played;return newMetadata;}
musicdb.onupgrading=function(event){App.showOverlay('upgrade');};musicdb.onunavailable=function(event){stopPlayingAndReset();var why=event.detail;if(why===MediaDB.NOCARD){App.showOverlay('nocard');}else if(why===MediaDB.UNMOUNTED){App.showOverlay('pluggedin');}};musicdb.oncardremoved=stopPlayingAndReset;function stopPlayingAndReset(){if(typeof PlayerView!=='undefined'){PlayerView.stop();}
if(!App.pendingPick){TabBar.option='title';ModeManager.start(MODE_LIST);}}
musicdb.onenumerable=startupOnEnumerable;function startupOnEnumerable(){if(App.currentOverlay==='upgrade'){App.showOverlay(null);}
App.showCurrentView(function(){reparsingMetadata=false;if(document.URL.indexOf('#pick')===-1){MusicComms.init();}
if(musicdb.state===MediaDB.READY){startupOnReady();}
else{musicdb.onready=startupOnReady;}});}
function startupOnReady(){if(App.currentOverlay==='nocard'||App.currentOverlay==='pluggedin'){App.showOverlay(null);}
musicdb.scan();musicdb.onready=postStartupOnReady;}
function postStartupOnReady(){if(App.currentOverlay==='nocard'||App.currentOverlay==='pluggedin'){App.showOverlay(null);}
App.showCurrentView(function(){musicdb.scan();});}
var filesDeletedWhileScanning=0;var filesFoundWhileScanning=0;var filesFoundBatch=0;var scanning=false;var SCAN_UPDATE_BATCH_SIZE=25;var DELETE_BATCH_TIMEOUT=500;var deleteTimer=null;musicdb.onscanstart=function(){scanning=true;filesFoundWhileScanning=0;filesFoundBatch=0;filesDeletedWhileScanning=0;};musicdb.onscanend=function(){scanning=false;if(filesFoundBatch>0||filesDeletedWhileScanning>0){filesFoundWhileScanning=0;filesFoundBatch=0;filesDeletedWhileScanning=0;App.showCurrentView();}
if(!firstScanDone){firstScanDone=true;window.performance.mark('fullyLoaded');window.dispatchEvent(new CustomEvent('moz-app-loaded'));}};musicdb.oncreated=function(event){let musicCount=window.localStorage.getItem('musicCount');window.localStorage.setItem('musicCount',musicCount+1);if(scanning){var currentMode=ModeManager.currentMode;var n=event.detail.length;filesFoundWhileScanning+=n;filesFoundBatch+=n;if(filesFoundBatch>SCAN_UPDATE_BATCH_SIZE){filesFoundBatch=0;App.showCurrentView();}}else{App.showCurrentView();}};musicdb.ondeleted=function(event){let musicCount=window.localStorage.getItem('musicCount');window.localStorage.setItem('musicCount',Number(musicCount)-1);let musicAlbum=window.localStorage.getItem('musicAlbum');window.localStorage.setItem('musicAlbum',Number(musicAlbum)-1);let musicArtist=window.localStorage.getItem('musicArtist');window.localStorage.setItem('musicArtist',Number(musicArtist)-1);if(scanning){filesDeletedWhileScanning+=event.detail.length;}
else{if(typeof PlayerView!=='undefined'){var deletedItem=event.detail[0];if(PlayerView.playSongName===deletedItem&&PlayerView.playStatus==='PLAYING'){PlayerView.next();}
var filter=PlayerView.dataSource.filter(function(elem){return elem.name!==deletedItem;});if(PlayerView.currentIndex>=1){PlayerView.currentIndex--;}
PlayerView.dataSource=filter;}
if(deleteTimer){clearTimeout(deleteTimer);}
deleteTimer=setTimeout(function(){deleteTimer=null;App.showCurrentView();},DELETE_BATCH_TIMEOUT);}};}