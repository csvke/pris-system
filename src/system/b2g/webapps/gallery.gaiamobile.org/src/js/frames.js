// This file contains Gallery code related to the fullscreen view

'use strict';

var frames = $('frames');

// These three objects are holders for the previous, current and next
// photos to be displayed. They get swapped around and
// reused when we pan to the next or previous photo: next becomes
// current, current becomes previous etc.  See nextFile() and
// previousFile().  Note also that the Frame object is not a DOM
// element.  Use currentFrame.container to refer to the section
// element. The frame constructor creates an <img> element,
// and you can refer to currentFrame.image .
var maxImageSize = CONFIG_MAX_IMAGE_PIXEL_SIZE;
var previousFrame = new MediaFrame($('frame1'), false, maxImageSize);
var currentFrame = new MediaFrame($('frame2'), false, maxImageSize);
var nextFrame = new MediaFrame($('frame3'), false, maxImageSize);

if (CONFIG_REQUIRED_EXIF_PREVIEW_WIDTH) {
  previousFrame.setMinimumPreviewSize(CONFIG_REQUIRED_EXIF_PREVIEW_WIDTH,
                                      CONFIG_REQUIRED_EXIF_PREVIEW_HEIGHT);
  currentFrame.setMinimumPreviewSize(CONFIG_REQUIRED_EXIF_PREVIEW_WIDTH,
                                     CONFIG_REQUIRED_EXIF_PREVIEW_HEIGHT);
  nextFrame.setMinimumPreviewSize(CONFIG_REQUIRED_EXIF_PREVIEW_WIDTH,
                                  CONFIG_REQUIRED_EXIF_PREVIEW_HEIGHT);
}

// When this variable is set to true, we ignore any user gestures
// so we don't try to pan or zoom during a frame transition.
var transitioning = false;

var frameOffset = 0; // how far are the frames swiped side-to-side?

window.addEventListener('moveframe', moveFrameHandler);

// Each of the Frame container elements may be subject to animated
// transitions. So give them transitionend event handlers that
// remove the transition style property when the transition ends.
// This helps prevent unexpected transitions.
function removeTransition(event) {
  event.target.style.transition = null;
}

previousFrame.container.addEventListener('transitionend', removeTransition);
currentFrame.container.addEventListener('transitionend', removeTransition);
nextFrame.container.addEventListener('transitionend', removeTransition);

// Clicking the delete button while viewing a single item deletes that item
function deleteSinglePhoto() {
  var msg = 'delete-photo?';

  LazyLoader.load('shared/js/dialogs.js', function() {
    Dialogs.confirm({
      messageId: msg,
      cancelId: 'cancel',
      confirmId: 'delete',
      danger: true,
      bodyClass: 'showing-dialog'
    }, function() { // onSuccess
      // disable delete, edit and share button to prevent
      // operations while delete item
      fullscreenButtons.delete.classList.add('disabled');
      fullscreenButtons.share.classList.add('disabled');
      fullscreenButtons.edit.classList.add('disabled');

      deleteFile(currentFileIndex);
    }, function() {
      // onCancel
    });
  })

}

// In fullscreen mode, the share button shares the current item
function shareSingleItem() {
  // This is the item we're sharing
  var fileinfo = files[currentFileIndex];
  
  // Otherwise it is an image.
  // If it does not have any EXIF orientation, and if we don't need
  // to downsample it, then just share it as it is.
  if (!fileinfo.metadata.rotation && !fileinfo.metadata.mirrored && !CONFIG_MAX_PICK_PIXEL_SIZE) {
    share([currentFrame.imageblob]);
  } else {
    // This is only tricky case. If we are sharing an image that uses
    // EXIF orientation for correct display, rotate it before sharing
    // so that the recieving app doesn't have to know about EXIF
    LazyLoader.load(['shared/js/media/downsample.js', 'shared/js/media/crop_resize_rotate.js'], shareModifiedImage);
  }

  function shareModifiedImage() {
    var metadata = fileinfo.metadata;
    Spinner.show();
    var maxsize = CONFIG_MAX_PICK_PIXEL_SIZE || CONFIG_MAX_IMAGE_PIXEL_SIZE;
    cropResizeRotate(currentFrame.imageblob, null,
                     maxsize || null, null, metadata,
                     function(error, rotatedBlob) {
                       if (error) {
                         console.error('Error while rotating image: ', error);
                         rotatedBlob = currentFrame.imageblob;
                       }
                       
                      LazyLoader.load('js/ensure_file_backed_blob.js', function() {
                        ensureFileBackedBlob(rotatedBlob, function(file) {
                          Spinner.hide();
                          share([file], currentFrame.imageblob.name);
                        });
                      });
                     });
  }
}

// Resize all the frames' content, if its container's size is changed
function resizeFrames() {
  nextFrame.reset();
  previousFrame.reset();
  currentFrame.reset();
}

// transform handler for new keyboard engine
function moveFrameHandler(e) {
  if (photodb.parsingBigFiles)
    return;
  var deltax = 6
  switch(e.detail.keyId) {
    case 0:
      currentFrame.pan(deltax,0);
      break;
    case 1:
      currentFrame.pan(-deltax,0);
      break;
    case 2:
      currentFrame.pan(0,deltax);
      break;
    case 3:
      currentFrame.pan(0,-deltax);
      break;
  }
}

// A utility function to display the nth image in the specified frame
// Used in showFile(), nextFile() and previousFile().
function setupFrameContent(n, frame) {
  // Make sure n is in range
  if (n < 0 || n >= files.length) {
    frame.clear();
    delete frame.filename;
    return;
  }

  var fileinfo = files[n];

  // If we're already displaying this file in this frame, then do nothing
  if (fileinfo.name === frame.filename) {
    return;
  }

  // Remember what file we're going to display
  frame.filename = fileinfo.name;

  photodb.getFile(fileinfo.name, function(imagefile) {
    frame.displayImage(
      imagefile,
      fileinfo.metadata.width,
      fileinfo.metadata.height,
      fileinfo.metadata.preview,
      fileinfo.metadata.rotation,
      fileinfo.metadata.mirrored,
      fileinfo.metadata.largeSize);
  });
}

var FRAME_BORDER_WIDTH = 3;

function setFramesPosition() {
  var width = window.innerWidth + FRAME_BORDER_WIDTH;
  currentFrame.container.style.transform =
    'translateX(' + frameOffset + 'px)';
  if (navigator.mozL10n.language.direction === 'ltr') {
    nextFrame.container.style.transform =
      'translateX(' + (frameOffset + width) + 'px)';
    previousFrame.container.style.transform =
      'translateX(' + (frameOffset - width) + 'px)';
  }
  else {
    // For RTL languages we swap next and previous sides
    nextFrame.container.style.transform =
      'translateX(' + (frameOffset - width) + 'px)';
    previousFrame.container.style.transform =
      'translateX(' + (frameOffset + width) + 'px)';
  }

  // XXX Bug 1021782 add 'current' class to currentFrame
  nextFrame.container.classList.remove('current');
  previousFrame.container.classList.remove('current');
  currentFrame.container.classList.add('current');

  // Hide adjacent frames from screen reader
  nextFrame.container.setAttribute('aria-hidden', true);
  previousFrame.container.setAttribute('aria-hidden', true);
  currentFrame.container.removeAttribute('aria-hidden');
}

function resetFramesPosition() {
  frameOffset = 0;
  setFramesPosition();
}

// Switch from thumbnail list view to single-picture fullscreen view
// and display the specified file.
function showFile(n) {
  // Mark what we're focusing on and unmark the old one
  updateFocusThumbnail(n);
  updateFrames();
}

// show or hide lock icon
function resetCurrentLockStatus () {
  // if lock div not created ,create it
  if (!currentFrame.container.lockStatusNode) {
    createFullscreenLockStatusIcon();
  }
  currentFrame.container.lockStatusNode.reset(chkIsCurrentLocked() ? true : false);
}

function createFullscreenLockStatusIcon() {
  var lockStatusNode = document.createElement('div');
  lockStatusNode.classList.add('fullscreen-lock-status');
  lockStatusNode.classList.add('gaia-icon');
  lockStatusNode.classList.add('icon-lock');
  lockStatusNode.reset = function (locked) {
    this.style.display = locked ? 'block' : 'none';
  }
  currentFrame.container.lockStatusNode = lockStatusNode;
  currentFrame.container.appendChild(lockStatusNode);
}

function updateFrames() {
  var n = currentFileIndex;
  setupFrameContent(Gallery.getPrevFileIndex(false), previousFrame);
  setupFrameContent(n, currentFrame);
  setupFrameContent(Gallery.getNextFileIndex(false), nextFrame);

  resetFramesPosition();

  resetCurrentLockStatus();
}

function clearFrames() {
  previousFrame.clear();
  currentFrame.clear();
  nextFrame.clear();
  delete previousFrame.filename;
  delete currentFrame.filename;
  delete nextFrame.filename;
}

// Transition to the next file, animating it over the specified time (ms).
// This is used when the user pans.
function nextFile(time) {
  // If already displaying the last one, do nothing.
  if (Gallery.currentFocusIndex === files.length - 1) {
    return;
  }
  var nextFileIndex = Gallery.getNextFileIndex(false);
  if (currentFileIndex === nextFileIndex) {
    return;
  }
  
  // Set a flag to ignore pan and zoom gestures during the transition.
  transitioning = true;
  setTimeout(function() { transitioning = false; }, time);

  // Set transitions for the visible frames
  var transition = 'transform ' + time + 'ms ease';
  currentFrame.container.style.transition = transition;
  nextFrame.container.style.transition = transition;

  // Cycle the three frames so next becomes current,
  // current becomes previous, and previous becomes next.
  var tmp = previousFrame;
  previousFrame = currentFrame;
  currentFrame = nextFrame;
  nextFrame = tmp;
  currentFileIndex = Gallery.getNextFileIndex(true);
  nextFileIndex = Gallery.getNextFileIndex(false);
  updateFocusThumbnail(currentFileIndex);
  // Move (transition) the frames to their new position
  resetFramesPosition();

  // Update the frame for the new next item
  setupFrameContent(nextFileIndex, nextFrame);

  // When the transition is done, cleanup
  currentFrame.container.addEventListener('transitionend', function done(e) {
    this.removeEventListener('transitionend', done);

    // Reposition the item that just transitioned off the screen
    // to reset any zooming and panning
    previousFrame.reset();
  });
  resetCurrentLockStatus();
}

function moveToPrev(cur_pos) {
  var pos_save = cur_pos;
  if (cur_pos === 0) {
    return 0;
  }
  while(true) {
    cur_pos = cur_pos -1;
    if (files[cur_pos].metadata.largeSize != true) {
      return cur_pos;
    }
    else {
      if (cur_pos === 0) {
        return pos_save;
      }
    }
  }
}
function moveToNext(cur_pos,max_length) {
  var pos_save = cur_pos;
  if (cur_pos >= max_length - 1) {
    return cur_pos;
  }
  while(true) {
    cur_pos = cur_pos + 1;
    if (files[cur_pos].metadata.largeSize != true) {
      return cur_pos;
    }
    else {
      if (cur_pos >= max_length - 1) {
        return pos_save;
      }
    }
  }
}
// Just like nextFile() but in the other direction
function previousFile(time) {
  // if already displaying the first one, do nothing.
  if (Gallery.currentFocusIndex === 0) {
    return;
  }
  var previousFileIndex = Gallery.getPrevFileIndex(false);
  if (currentFileIndex === previousFileIndex) {
    return;
  }

  // Set a flag to ignore pan and zoom gestures during the transition.
  transitioning = true;
  setTimeout(function() { transitioning = false; }, time);

  // Set transitions for the visible frames
  var transition = 'transform ' + time + 'ms ease';
  previousFrame.container.style.transition = transition;
  currentFrame.container.style.transition = transition;

  // Transition to the previous item: previous becomes current, current
  // becomes next, etc.
  var tmp = nextFrame;
  nextFrame = currentFrame;
  currentFrame = previousFrame;
  previousFrame = tmp;
  currentFileIndex = Gallery.getPrevFileIndex(true);
  previousFileIndex = Gallery.getPrevFileIndex(false);
  updateFocusThumbnail(currentFileIndex);
  // Move (transition) the frames to their new position
  resetFramesPosition();

  // Preload the new previous item
  setupFrameContent(previousFileIndex, previousFrame);

  // When the transition is done do some cleanup
  currentFrame.container.addEventListener('transitionend', function done(e) {
    this.removeEventListener('transitionend', done);
    // Reset the size and position of the item that just panned off
    nextFrame.reset();
  });
  resetCurrentLockStatus();
}
var ZOOM_IN_LIMIT = 9;
var ZOOM_OUT_LIMIT = 0;
var ZOOM_GRADE_RATIO = 0.09;

function zoomOut(){
  var originScale = parseFloat(currentFrame.image.getAttribute('data-origin-scale'));
  var scaleDelta = parseFloat(currentFrame.image.getAttribute('data-scale-delta'));
  var zoomInTimes = parseInt(currentFrame.image.getAttribute('data-zoom-in'));
  var zoomOutTimes = parseInt(currentFrame.image.getAttribute('data-zoom-out'));
  var scale = currentFrame.fit.scale;
  if(zoomOutTimes >= ZOOM_OUT_LIMIT) {
    return false;
  }
  zoomOutTimes ++;
  zoomInTimes --;
  updateZoomData(zoomInTimes,zoomOutTimes);
  scale=scale - scaleDelta;
  currentFrame.zoomFrame(scale, 0, 0, 200);
  return zoomOutTimes >= ZOOM_OUT_LIMIT?false:true;
}
function zoomIn(){
  var originScale = parseFloat(currentFrame.image.getAttribute('data-origin-scale'));
  var scaleDelta = parseFloat(currentFrame.image.getAttribute('data-scale-delta'));
  var zoomInTimes = parseInt(currentFrame.image.getAttribute('data-zoom-in'));
  var zoomOutTimes = parseInt(currentFrame.image.getAttribute('data-zoom-out'));
  var scale = currentFrame.fit.scale;
  if(zoomInTimes >= ZOOM_IN_LIMIT) {
    return false;
  }
  zoomInTimes ++;
  zoomOutTimes --;
  updateZoomData(zoomInTimes,zoomOutTimes);
  scale=scale + scaleDelta;
  currentFrame.zoomFrame(scale, 0, 0, 200);
  return zoomInTimes >= ZOOM_IN_LIMIT?false:true;
}
// update the attribute value of zoom data after we zoom in/out
function updateZoomData(inTimes,outTimes) {
  currentFrame.image.setAttribute('data-zoom-in',inTimes);
  currentFrame.image.setAttribute('data-zoom-out',outTimes);
}
// restore photo when back from zoom mode
function restoreFrame() {
  var originalScale = currentFrame.image.getAttribute('data-origin-scale');
  updateZoomData(0,0);
  currentFrame.zoomFrame(parseFloat(originalScale), 0, 0, 200);
}
