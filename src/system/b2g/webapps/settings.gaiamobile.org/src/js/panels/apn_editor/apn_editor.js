/**
 * The apn editor module
 */
define(['require','panels/apn_editor/apn_editor_const','panels/apn_editor/apn_editor_session'],function(require) {
  

  var ApnEditorConst = require('panels/apn_editor/apn_editor_const');
  var ApnEditorSession = require('panels/apn_editor/apn_editor_session');

  var APN_PROPERTIES = ApnEditorConst.APN_PROPERTIES;
  var APN_PROPERTY_DEFAULTS = ApnEditorConst.APN_PROPERTY_DEFAULTS;
  var VALUE_CONVERTERS = ApnEditorConst.VALUE_CONVERTERS;
  var isPreset = false;

  function ApnEditor(rootElement) {
    this._inputElements = {};
    APN_PROPERTIES.forEach(function(name) {
      this._inputElements[name] = rootElement.querySelector('.' + name);
    }, this);
  }

  ApnEditor.prototype = {
    _convertValue: function ae_convertValue(value, converter) {
      if (converter) {
        return converter(value);
      } else {
        return value;
      }
    },
    _fillInputElements: function ae_fillInputElements(inputElements, apn) {
      navigator.customization.getValue('apn.protocol.type').then((result) => {
        var protocolType = `${JSON.stringify(result)}`;
        if(protocolType == 'undefined') {
            protocolType = 'IP';
            console.log('apn_editor.js  apn.protocol.type not defined');
        }
        if(protocolType == '0') {
            protocolType = 'IP';
        }
        if(protocolType == '1') {
            protocolType = 'IPV6';
        }
        if(protocolType == '2') {
            protocolType = 'IPV4V6';
        }
        APN_PROPERTIES.forEach(function(name) {
          var inputElement = inputElements[name];
          if (inputElement) {
            var value = (apn && apn[name.toLowerCase()]) ||
                  APN_PROPERTY_DEFAULTS[name];
            //Defect4171-tf-zhang@t2mobile.com-modify
            //if( (name == 'protocol' || name == 'roaming_protocol')
            //    && isPreset){
            //  inputElement.value = protocolType;
            //}else{
              inputElement.value =
                this._convertValue(value, VALUE_CONVERTERS.TO_STRING[name]);
            //}
          }
        }, this);
      });
    },
    createApn: function ae_createApn(serviceId, apnItem) {
      isPreset = (apnItem['category'] == 'preset');
      this._fillInputElements(this._inputElements, apnItem.apn);
      return ApnEditorSession(serviceId, 'new', this._inputElements, apnItem);
    },
    editApn: function ae_editApn(serviceId, apnItem) {
      isPreset = (apnItem['category'] == 'preset');
      this._fillInputElements(this._inputElements, apnItem.apn);
      return ApnEditorSession(serviceId, 'edit', this._inputElements, apnItem);
    }
  };

  return function ctor_apn_editor(rootElement) {
    return new ApnEditor(rootElement);
  };
});
