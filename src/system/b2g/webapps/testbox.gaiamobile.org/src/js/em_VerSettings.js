/*© 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED, all rights reserved.*/
// ************************************************************************
// * File Name: em_VerSettings.js
// * Description: engmode -> Local Customization: Version.
// * Note: dislay IMEI info and SW info
// ************************************************************************

'use strict';

$('menuItem-version').addEventListener('click', function() {
  em_VerSettings();
});

function em_VerSettings() {
  var elements = {};
  var verInfoPanel = $('version_infoPanel');

  function _init() {
    elements = {
      imeiItems: verInfoPanel.querySelector('.imeiInfo'),
      swInfoItem: verInfoPanel.querySelector('.swInfo')
    };

    _showImeiInfo();
    _showVersionInfo();
  }

  function _getImeiCode(simSlotIndex, conn) {
    return conn.getDeviceIdentities().then((deviceInfo) => {
      if (deviceInfo.imei) {
        return deviceInfo.imei;
      } else {
        var errorMsg = 'engmode em_VerSettings: Could not retrieve the IMEI code for SIM ' + simSlotIndex;
        dump(errorMsg);
        return Promise.reject(new Error(errorMsg));
      }
    });
  }

  function _createImeiField(imeis) {
    while (elements.imeiItems.hasChildNodes()) {
      elements.imeiItems.removeChild(elements.imeiItems.lastChild);
    }

    if (!imeis || imeis.length === 0) {
      var span = document.createElement('span');
      span.textContent = 'IMEI is not available.'
    } else {
      imeis.forEach(function(imei, index) {
        var span = document.createElement('span');
        if (imeis.length > 1) {
          span.textContent = 'IMEI ' + index + ': <bdi>' + imei + '</bdi>'
        } else {
          span.textContent = 'IMEI: ' + imei;
        }
        elements.imeiItems.appendChild(span);
      });
    }
  }

  function _showImeiInfo() {
    var conns = navigator.mozMobileConnections;
    if (!navigator.mozTelephony || !conns) {
      dump('engmode em_VerSettings error: mozTelephony or mozMobileConnections not support.');
      elements.imeiItems.innerHTML = 'IMEI is not available.';
      return;
    }

    var promises = [];
    for (var i = 0; i < conns.length; i++) {
      promises.push(_getImeiCode(i, conns[i]));
    }

    Promise.all(promises).then((imeis) => {
      _createImeiField(imeis);
    }, function() {
      _createImeiField(null);
    });
  }

  function _showVersionInfo() {
    if (!navigator.engmodeExtension) {
      elements.swInfoItem.innerHTML = 'SW Version: KaiOS extension is not available.';
      return;
    }

    var swInfo = navigator.engmodeExtension.getPropertyValue('ro.build.display.id');
    elements.swInfoItem.innerHTML = 'SW Version: ' + swInfo;
  }

  return _init();
}
