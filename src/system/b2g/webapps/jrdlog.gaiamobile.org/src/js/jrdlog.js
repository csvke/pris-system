'use strict';

//script of jrdlog app
(function(win, undefined) {

  //log position key
  const LOG_POSITION_KEY = 'logPosition';

  //log position
  var logPosition;

  //JRD EXTENSION
  var kaiosExtension = navigator.kaiosExtension;
  var isfileExit = false;
  var DEBUG = 1;
  var isProcessInitating = false;
  var _settings = window.navigator.mozSettings;
  var debug = function(s) {
  };
  if (DEBUG) {
    debug = function(s) {
      dump('<JRD_LOG> -*- jrdLog -*-: ' + s + '\n');
      //console.log('<JRD_LOG> -*- jrdLog -*-: ' + s + '\n');
    };
  }

  //keys in bugreport.html,dmesg.html,logcat.html,qxdm.html,tombstones.html
  //the pages have the similar structure
  //maybe need some configuration
  const LOG_TYPE_KEYS = ['tcpdump', 'logcat', 'qxdm', 'dmesg', 'initlog',
    'geckolog', 'versioninfo', 'settingsapnvalue', 'nvvalue', 'propvalue',
    'clearlogs', 'tombstones', 'bugreport', 'crashlog'];

  const LOG_INIT_KEYS = ['dmesg', 'qxdm', 'logcat', 'tcpdump'];

  var self = this || win;
  //get element by id
  function $(id) {
    return document.getElementById(id);
  }

  function loadJSON(href, callback) {
    if (!callback)
      return;
    var xhr = new XMLHttpRequest();
    xhr.onerror = function() {
      console.error('Failed to fetch file: ' + href, xhr.statusText);
    };
    xhr.onload = function() {
      callback(xhr.response);
    };
    xhr.open('GET', href, true); // async
    xhr.responseType = 'json';
    xhr.send();
  }
  //get radio selected value
  function getRadioValue(name) {
    var radios = document.getElementsByName(name);
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].checked) {
        return radios[i].value;
      }
    }
  }

  //set the radio value by it's name
  function setRadioValue(name, value) {
    var checkedRadios;
    var radios = document.getElementsByName(name);
    for (var i = 0; i < radios.length; i++) {
      if (radios[i].value === value) {
        radios[i].checked = 'checked';
        checkedRadios = radios[i];
      } else {
        radios[i].checked = '';
      }
    }
    return checkedRadios;
  }

  function getCustlogStatus(key) {

    if('mms' === key) {
      var strCommand = 'mms.debugging.enabled';
      var value = kaiosExtension.readRoValue(strCommand);
      if('true' === value) {
        return true;
      }else{
        return false;
      }
    } else if ('fota' === key) {
      var keystr = 'persist.sys.fotalog.enabled';
      var request = _settings.createLock().get(keystr);
      request.onsuccess = function() {
        if (true == request.result[keystr]) {
          $('fotalog-input').checked = true;
          return true;
        }else {
          $('fotalog-input').checked = false;
          return false;
        }
      };
    }
  }
  function setCustlogStatus(key, value) {
    if('mms' === key) {
      var strCommand = 'mms_debugging';
      if (kaiosExtension) {
        var initRequest = kaiosExtension.setPropertyLE(strCommand, value);
        initRequest.onsuccess = function() {
          return true;
        };
      }
    } else if ('fota' === key) {
      var keystr = 'persist.sys.fotalog.enabled';
      var cset = {};
      cset[keystr] = value;
      _settings.createLock().set(cset);
    }
  }

  function getdiagcfgStatus() {
    var value = kaiosExtension.readRoValue('persist.sys.kaiosdiagcfg.enable');
    if('1' === value) {
      return true;
    }else{
      return false;
    }
  }
  function setdiagcfgStatus(value) {
    var autoCommand = 'kaios_diagcfg';
    if (kaiosExtension) {
      var mValue;
      if(true == value) {
        mValue = 1;
      } else {
        mValue = 0;
      }
      var initRequest = kaiosExtension.setPropertyLE(autoCommand, mValue);
      initRequest.onsuccess = function() {
        return true;
      };
    }
  }

  function checkfilesExit(path) {
    var req = kaiosExtension.checkIsFileExist(path);
    req.onsuccess = function(e){
      if('EXIST' == e.target.result) {
        isfileExit = true;
      } else if ('NO_EXIST' == e.target.result) {
        isfileExit = false;
      }
    }
  }
  //include saving last modification time and position
  function onSuccess(key, request) {
    if (undefined == request.result[key]) {
      debug(JSON.stringify(request));
      return;
    }

    var date = request.result[key].timeStr;
    var relPosition = request.result[key].location;
    $(key + 'MdfTime').innerHTML = date;
    $(key + 'Position').innerHTML = relPosition;
    $(key + 'MdfTime').style.opacity = 1;
    $('loading').style.display = 'none';

    debug(JSON.stringify(request.result[key]));
  }

  function addfocuslist(){
    window.setTimeout(function() {
      var focusElements = document.querySelectorAll('[data-type="focusEnabled"]');
      debug('addfocuslist focusElements = ' + focusElements.toString());
      for(var item in focusElements) {
        if('object' == typeof(focusElements[item])) {
          focusElements[item].jrdFocus = true;
          focusElements[item].tabIndex = 1;
          debug('addfocuslist ' + item + ' = ' + focusElements[item].id);
          focusElements[0].focus();
        }
      }
    }, 200);
  }

  window.addEventListener('keydown',
    function initaddEventListerner(e){
      var cmd = ConvertToCmd(e.key);
      debug('addEventListener jrdlog e.key = ' + e.key);
      if (e.key && ('Enter' == e.key)) {
        debug('addEventListener 1 cmd = ' + cmd)
        if (('kaioscmYes' == cmd) ||  ('kaioscmOk' == cmd)){
          var el = document.activeElement;
          if (el) {
            el.click();
            debug('addEventListener jrdlog el.name = ' + el.id);
          }
        }
      } else if(e.key && ('Backspace' == e.key)) {
          debug('addEventListener 2 cmd = ' + cmd);
          var isOpened = parent.document.body.classList.contains('pageChange');
          if (!isOpened) {
              debug('addEventListener 2 click back to home ');
              window.close();
          } else {
              debug('addEventListener 2 back ');
              parent.document.getElementById('window').style.display = 'block';
              win.setTimeout(function openTestPage() {
                  var mvalue = document.getElementById('home2');
                  dump('addEventListener::document.mvalue id = ' + mvalue.id + ' value = ' + mvalue.value);
                  parent.document.body.classList.remove('pageChange');
                  parent.document.getElementById('nextWindow').style.display = 'none';
                  parent.document.getElementById('nextFrame').src = '';

                  dump('addEventListener:document.activeElement ' + document.activeElement.id);
                  if(null != mvalue) {
                      parent.document.getElementById(mvalue.value).focus();
                  } else {
                      parent.document.getElementById('logPositionitem0').focus();
                  }
                  dump('addEventListener after : document.activeElement ' + document.activeElement.id);
              }, 200);
              e.preventDefault();
          }
      }
      // else if(e.key && ('BrowserBack' == e.key)) {
      //   debug('gaolu 2 cmd = ' + cmd);
      //   if ('kaioscmNo' == cmd) {
      //     var isOpened = parent.document.body.classList.contains('pageChange');
      //     if (!isOpened) {
      //       debug('gaolu 2 click back to home ');
      //       window.close();
      //     } else {
      //       debug('gaolu 2 back ');
      //       parent.document.getElementById('window').style.display = 'block';
      //       win.setTimeout(function openTestPage() {
      //         var mvalue = document.getElementById('home2');
      //         dump('gaolu::document.mvalue id = ' + mvalue.id + ' value = ' + mvalue.value);
      //         parent.document.body.classList.remove('pageChange');
      //         parent.document.getElementById('nextWindow').style.display = 'none';
      //         parent.document.getElementById('nextFrame').src = '';
      //
      //         dump('gaolu:document.activeElement ' + document.activeElement.id);
      //         if(null != mvalue) {
      //           parent.document.getElementById(mvalue.value).focus();
      //         } else {
      //           parent.document.getElementById('logPositionitem0').focus();
      //         }
      //         dump('gaolu after : document.activeElement ' + document.activeElement.id);
      //       }, 200);
      //     }
      //   }
      // }
    });

  function init() {
    logPosition = getPosition();
    setRadioValue('logPosition', logPosition);
    for (var i = 0; i < LOG_TYPE_KEYS.length; i++) {
      if ($('window_' + LOG_TYPE_KEYS[i])) {
        //excute when page init
        var key = LOG_TYPE_KEYS[i];
        refeshfiletimes(key);
        if (!kaiosExtension) {
          break;
        }
        if ('qxdm' === key || 'logcat' === key ||
          'dmesg' === key || 'tcpdump' === key ||
          'geckolog' === key || 'initlog' === key) {
          var strPersist = 'persist.sys.kaios' + key + '.enable';
          var value = kaiosExtension.readRoValue(strPersist);
          if ('0' == value || '' == value) {
            $('loading').style.display = 'none';
            $(key + 'Run').innerHTML = 'Run';
          } else {
            debug('jrdlog init loading...');
            win.setTimeout(function openTestPage() {
              $('loading').style.display = 'block';
              $(key + 'Run').innerHTML = 'Stop';
            }, 300);
          }
          // if('logcat' === key) {
          //   //commented out by elegentin.xie
          //   // setProps('debug.console.enabled', true);
          //   // setProps('wifi.debugging.enabled', true);
          //   //commented out by elegentin.xie
          //   var mmslogsStatus = getCustlogStatus('mms');
          //   if(mmslogsStatus) {
          //     $('mmslog-input').checked = true;
          //   } else {
          //     $('mmslog-input').checked = false;
          //   }
          //   $('mmslog-input').addEventListener('change', function(evt) {
          //     var input = evt.target;
          //     var value = input.checked;
          //     setCustlogStatus('mms', value);
          //   });
          //
          //   getCustlogStatus('fota');
          //
          //   $('fotalog-input').addEventListener('change', function(evt) {
          //     var input = evt.target;
          //     var value = input.checked;
          //     setCustlogStatus('fota', value);
          //   });
          // }
          if('qxdm' === key) {
            var diagcfgStatus = getdiagcfgStatus();
            if(true == diagcfgStatus) {
              $('diagcfg-input').checked = true;
              $('diagcomm').innerHTML =
                'diag_mdlog -f /storage/sdcard/diag.cfg';
            } else {
              $('diagcfg-input').checked = false;
            }
            checkfilesExit('/storage/sdcard/diag.cfg');
            $('diagcfg-input').addEventListener('change', function(evt) {
              var input = evt.target;
              var value = input.checked;
              if (true == value) {
                if(true == isfileExit) {
                  $('diagcomm').innerHTML =
                    'diag_mdlog -f /storage/sdcard/diag.cfg';
                    setdiagcfgStatus(value);
                }else{
                  $('diagcfg-input').checked = false;
                  alert('The customized diag.cfg is\'t exit');
                }
              } else {
                $('diagcomm').innerHTML = 'diag_mdlog -f /vendor/diag.cfg';
                setdiagcfgStatus(value);
              }
            });
          }
          if('initlog' === key) {
            var value = kaiosExtension.readRoValue('persist.sys.initkeys.enable');
            debug('init initlog persist.sys.initkeys.enable = ' + value);
            //add by haibo.chen1@t2mobile.com for Bug 419 2017-10-26 begin
            //no default value should be given.
            if(value == ''){
                value = 'dmesginput';
                var autoCommand = 'kaios_initkeys';
                if (kaiosExtension) {
                   var initRequest = kaiosExtension.setPropertyLE(autoCommand, value);
                   initRequest.onsuccess = function() {
                     debug("setPropertyLE successfully: value = " + value);
                     return true;
                   };
                }
             }
            //add by haibo.chen1@t2mobile.com for Bug 419 2017-10-26 end
            for (var j = 0; j < LOG_INIT_KEYS.length; j++) {
              var Iint_keys = $(LOG_INIT_KEYS[j] + 'input');
              if(Iint_keys) {
                if(Iint_keys.id === value) {
                  Iint_keys.checked = true;
                }else{
                  Iint_keys.checked = false;
                }
              }
            }
          }
        }
      }
    }
  }

  //save the log position
  function savePosition() {
    logPosition = getRadioValue('logPosition');
    localStorage.setItem(LOG_POSITION_KEY, logPosition);
  }

  //get the log position
  function getPosition() {
    var value = localStorage.getItem(LOG_POSITION_KEY);
    debug('getPosition value : ' + value);
    if (!value) {
      value = 'INTERNAL_SDCARD';   //change phone to internal_sdcard bug #641051
    }
    return value;
  }

  //bound the event of tags when page init
  function fireEvent() {
    if (!kaiosExtension) {
      debug('Jrd Extension is not support.');
    }

    //transition when page change
    function changePage(name) {
      $('nextFrame').src = name + '.html';
      $('nextWindow').style.display = 'block';
      win.setTimeout(function openTestPage() {
        document.body.classList.add('pageChange');
        $('window').style.dislply = 'none';
        win.scrollTo(0, 0);
      }, 200);
    }

    //congqb@tcl.com add for external sdcard check
      function changePageOrAlert(name) {
        debug('changePageOrAlert');
        var mPosition = getPosition();
    //change phone to internal_sdcard && sdcard to external_sdcard bug #641051
          if (mPosition === 'INTERNAL_SDCARD') {
            changePage(name);
          }else if (mPosition === 'EXTERNAL_SDCARD') {
            debug('external-sdcard-branch');
            isExternalSdCardAvailable(name);
          }else {
            alert('neither select EXTERNAL_SDCARD nor INTERNAL_SDCARD');
          }
      }

      //congqb@tcl.com add for external sdcard check
      function isExternalSdCardAvailable(name) {
          var storages = navigator.getDeviceStorages('sdcard');
          debug('storages.length : ' + storages.length);

          function isExternalSdcard(element , index, array) {
            debug("element.storageName: "+ element.storageName);
              if (element.storageName === 'sdcard')
                  return true;
              return false;
          }
          var index = storages.findIndex(isExternalSdcard);
          debug('storage--index : ' + index);
          if (index == -1) {
              alert('The INTERNAL_SDCARD SDcard on your device is not available');
          }else if(storages.length == 2){
              var request = storages[index].available();
              debug('request : ' + JSON.stringify(request));
              request.onsuccess = (function(e) {
                debug("storages[index].available() onsuccess");
                  // The result is a string
                  if (e.target.result === 'available') {
                      debug('The External SDCard on your device is available');
                      changePage(name);
                  } else if (e.target.result === 'unavailable') {
                      changePage(name);
                      debug('The External SDCard on your device is not available');//this External SDCard is for debug understand, for user it is called INTERNAL_SDCARD
                      alert("The INTERNAL_SDCARD on your device is not available, please reopen app");//this INTERNAL_SDCARD is for user understand
                  } else {
                      alert('The External SDCard on your device is ' +
                        'shared and thus not available');
                  }
              });
              request.onerror = function() {
                  alert('Unable to get the space used by the External SDCard ');
              };
              debug("elegentin storages.length is 2, now sdcard is named and used for internal_sdcard.")
          }else if(storages.length == 1){
            alert('The INTERNAL_SDCARD on your device is not available, please reopen app');
            debug("elegentin storages.length is 1, now sdcard is named for data.");
          }
      }

    //when click the radio button in index page
    //it will changes the log position of the system and save the status
    if($('logPositionitem0')) {
      $('logPositionitem0').addEventListener('click', function() {
        $('logPosition0').checked = 'checked';
        debug('radio1 checked');
        savePosition();
      });
    }
    if($('logPositionitem1')) {
      $('logPositionitem1').addEventListener('click', function() {
        $('logPosition1').checked = 'checked';
        debug('radio2 checked');
        savePosition();
      });
    }

    //when click the run button in page tombstones.html
    if ($('tombstones')) {
      $('tombstones').addEventListener('click', function() {
        debug('tombstons1')
          changePageOrAlert('tombstones');
        debug('tombstons2')
      });
    }//

    if ($('crashlog')) {
      $('crashlog').addEventListener('click', function() {
        changePageOrAlert('crashlog');
      });
    }

    //when click the run button in page bugreport.html
    if ($('bugreport')) {
      $('bugreport').addEventListener('click', function() {
          changePageOrAlert('bugreport');
      });
    }

    //when click the run button in page qxdm.html
    if ($('qxdm')) {
      $('qxdm').addEventListener('click', function() {

//        if(getPosition() === 'INTERNAL_SDCARD'){
//           alert('Please Store qxdm log to EXTERNAL SD Card');
//        }else
//      change phone to internal_sdcard && sdcard to external_sdcard bug #641051
            changePageOrAlert('qxdm');
      });
    }

    //when click the run button in page dmesg.html
    if ($('dmesg')) {
      $('dmesg').addEventListener('click', function() {
          changePageOrAlert('dmesg');

      });
    }

    //when click the run button in page logcat.html
    if ($('logcat')) {
      $('logcat').addEventListener('click', function() {
          changePageOrAlert('logcat');
      });
    }

    //when click the run button in page initlog.html
    if ($('initlog')) {
      $('initlog').addEventListener('click', function() {
        changePageOrAlert('initlog');
      });
    }
    //when click the run button in page geckolog.html
    if ($('geckolog')) {
      $('geckolog').addEventListener('click', function() {
        changePageOrAlert('geckolog');
      });
    }

    //when click the run button in page tcpdump.html
    if ($('tcpdump')) {
      $('tcpdump').addEventListener('click', function() {
        changePageOrAlert('tcpdump');
      });
    }

    //when click the run button in page versioninfo.html
    if ($('versioninfo')) {
      $('versioninfo').addEventListener('click', function() {
        changePageOrAlert('versioninfo');
      });
    }

    //when click the run button in page settingsvalue.html
    if ($('settingsapnvalue')) {
      $('settingsapnvalue').addEventListener('click', function() {
        changePageOrAlert('settingsapnvalue');
      });
    }

    //when click the run button in page nvvalue.html
    if ($('nvvalue')) {
      $('nvvalue').addEventListener('click', function() {
        changePageOrAlert('nvvalue');
      });
    }

    //when click the run button in page propvalue.html
    if ($('propvalue')) {
      $('propvalue').addEventListener('click', function() {
        changePageOrAlert('propvalue');
      });
    }

    //when click the run button in page clearlogs.html
    if ($('clearlogs')) {
      $('clearlogs').addEventListener('click', function() {
        changePageOrAlert('clearlogs');
      });
    }
    //when click the home button in all sub pages
    if ($('home')) {
      $('home').addEventListener('click', function() {
        parent.document.getElementById('window').style.display = 'block';
        win.setTimeout(function openTestPage() {
          var isOpened = parent.document.body.classList.contains('pageChange');
          if (!isOpened)
            return false;
          var mvalue = document.getElementById('home2');
          dump('home::document.mvalue id = ' + mvalue.id + ' value = ' + mvalue.value);
          parent.document.body.classList.remove('pageChange');
          parent.document.getElementById('nextWindow').style.display = 'none';
          parent.document.getElementById('nextFrame').src = '';

          dump('home::::::::::::::::::::: document.activeElement ' + document.activeElement.id);
          if(null != mvalue) {
            parent.document.getElementById(mvalue.value).focus();
          } else {
            parent.document.getElementById('logPositionitem0').focus();
          }

          dump('home after : document.activeElement ' + document.activeElement.id);
        }, 200);
      });
    }

  //add by haibo.chen1@t2mobile.com for Bug 419 2017-10-26 begin
  if ($('menuItem-dmsgdisplay')) {
      $('menuItem-dmsgdisplay').addEventListener('click', function() {
          setRadioValue('form1', 'dmesginput');
          var value = document.getElementById('dmesginput').checked;
          var autoCommand = 'kaios_initkeys';
          if (value) {
            if (kaiosExtension) {
              var initRequest = kaiosExtension.setPropertyLE(autoCommand, 'dmesginput');
              initRequest.onsuccess = function() {
                debug("setprop dmesginput sucessfully");
                return true;
               };
             }
           }
          });
    }

    if ($('menuItem-logcatdisplay')) {
        $('menuItem-logcatdisplay').addEventListener('click', function() {
            setRadioValue('form1', 'logcatinput');
            var value = document.getElementById('logcatinput').checked;
            var autoCommand = 'kaios_initkeys';
            if (value) {
              if (kaiosExtension) {
                var initRequest = kaiosExtension.setPropertyLE(autoCommand, 'logcatinput');
                initRequest.onsuccess = function() {
                  debug("setprop logcatinput sucessfully");
                  return true;
                };
              }
            }
        });
    }
    //add by haibo.chen1@t2mobile.com for Bug 419 2017-10-26 end
    //do something when click run button and save the time when success do this
    for (var i = 0; i < LOG_TYPE_KEYS.length; i++) {
      if ($(LOG_TYPE_KEYS[i] + 'Run')) {
        var key = LOG_TYPE_KEYS[i];
        debug('fireEvent for LOG_TYPE_KEYS[' + i + '] = ' + LOG_TYPE_KEYS[i]);
        $(key + 'Run').addEventListener('click', function() {
          if (false == isProcessInitating) {
            isProcessInitating = true;
            win.setTimeout(function() {
              isProcessInitating = false;
            },1500);
          }else {
            return;
          }
          if ($(key + 'Run2').value === 'tcpdump' ||
              $(key + 'Run2').value === 'logcat' ||
              $(key + 'Run2').value === 'qxdm' ||
              $(key + 'Run2').value === 'dmesg') {
            //beginruning(key);
            jrdlogKeyinfo(key);
          } else if ($(key + 'Run2').value === 'initlog') {
            initloginfo(key);
          } else if ($(key + 'Run2').value === 'geckolog') {
            jrdhttpGeckolog(key);
          } else if ($(key + 'Run2').value === 'versioninfo') {
            beginruning(key);
            Versioninfo(key);
          } else if ($(key + 'Run2').value === 'settingsapnvalue') {
            beginruning(key);
            SettingsAPN(key);
          } else if ($(key + 'Run2').value === 'nvvalue') {
            var SETTINGS_CHECKLIST = '/shared/resources/toolboxcustitems.json';
            beginruning(key);
            NvSettingsProp(key, SETTINGS_CHECKLIST, 'NV');
          } else if ($(key + 'Run2').value === 'propvalue') {
            var SETTINGS_CHECKLIST = '/shared/resources/toolboxcustitems.json';
            beginruning(key);
            NvSettingsProp(key, SETTINGS_CHECKLIST, 'Prop');
          } else if ($(key + 'Run2').value === 'clearlogs') {
            beginruning(key);
            clearalllogs(key);
          } else {
            onlyRun(key);
          }
        });

        if('initlog' === key) {
          for (var k = 0; k < LOG_INIT_KEYS.length; k++) {
            var Iint_keys = $(LOG_INIT_KEYS[k] + 'input');
            if(Iint_keys) {
              Iint_keys.addEventListener('change', function(evt) {
                debug('fireEvent for initlog evt.target = ' + evt.target);
                var input = evt.target;
                var value = input.checked;
                var autoCommand = 'kaios_initkeys';
                if (value) {
                  if (kaiosExtension) {
                    var initRequest =
                      kaiosExtension.setPropertyLE(autoCommand, input.id);
                    initRequest.onsuccess = function() {
                      return true;
                    };
                  }
                }
              });
            }
          }
        }
      }
    }

    var oldClick = '';
    //call jrd extention method run
    function callJrdRun(key, type, position) {
      debug('callJrdRun key = ' + key + ' type = ' + type);
      var mDevice = getstoragedevice();
      var path = mDevice + '/' + key;
      var request = kaiosExtension.createFileLE('DIRECTORY', path);
      request.onsuccess = function(e) {
        onSuccess(key, request);
      };
      request.onerror = function(e) {
        debug('error:' + JSON.parse(request.error.name).errorInfo + '\n');
        if (JSON.parse(request.error.name).errorType == 'SourceFileNotExist') {
          alert('Log file is not exist!');
        }else if (JSON.parse(request.error.name).errorType ==
          'IllegalFilePathToStoreLog') {
//         tcl_congqingbin "command type check failed callback"
            alert('Store log to a wrong place');
        }
        $('loading').style.display = 'none';
        $(key + 'Run').onclick = oldClick;
      };
    }

    //click button only need run process
    function onlyRun(key) {
      oldClick = $(key + 'Run').onclick;
      $(key + 'Run').onclick = '';
      $('loading').style.display = 'block';
      $(key + 'MdfTime').style.opacity = 0;
      //add out by elegentin.xie
      if ('logcat' == key) {
        setProps('debug.console.enabled', true);
        setProps('wifi.debugging.enabled', true);
      }
      //add out by elegentin.xie
      if (kaiosExtension) {
        debug('begin:' + $(key + 'Run2').value + ' position:' + logPosition);
        callJrdRun(key, $(key + 'Run2').value, logPosition);
      }
    }
  }

  function getTimes() {
    var tempDate = new Date();
    var date_str = tempDate.toLocaleFormat('%Y%m%d_%H%M%S');
    return date_str;
  }
  function beginruning(key) {

    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
    }else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      refeshfiletimes(key);
      return;
    }
  }
  function refeshfiletimes(key) {
    var parmArray = new Array();
    var mDevice = getstoragedevice();
    var keyastr;
    if ('qxdm' == key) {
      keyastr = 'diag';
    } else {
      keyastr = key;
    }
    var filepath = mDevice + '/' + key + '/' + keyastr + '*';
    parmArray.push(filepath);
    debug('refeshfiletimes :' + parmArray.toString());
    var request = kaiosExtension.getFilesLastTime('normal', parmArray, 1);
    request.onsuccess = function(e) {
      var date = request.result[keyastr].timeStr;
      var relPosition = request.result[keyastr].location;
      $(key + 'MdfTime').innerHTML = date;
      $(key + 'Position').innerHTML = relPosition;
      $(key + 'MdfTime').style.opacity = 1;

      $(key + 'Run').innerHTML = 'Run';
      $('loading').style.display = 'none';
      debug(JSON.stringify(request.result[keyastr]));
    };
    request.onerror = function() {
      debug('error:' + JSON.parse(request.error.name).errorInfo + '\n');
    };
  }
  //set debug.console.enabled = true;

  function jrdhttpGeckolog(key) {
    var mDevice = getstoragedevice();
    var time = getTimes();
    var path;
    var httpgeckologcmd = 'timestamp,nsHttp:5,' +
                          'nsSocketTransport:5,' +
                          'nsHostResolver:5';
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';

      path = mDevice + '/' + key + '/' + key + time + '.log';
      kaiosExtension.createFileLE('FILE', path);

      var parmArray = new Array();
      parmArray.push('path');
      parmArray.push(path);
      var initPathRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
      initPathRequest.onsuccess = function() {
        var parmArray = new Array();
        parmArray.push('cmd');
        parmArray.push(httpgeckologcmd);
        var initPathRequest1 = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
        initPathRequest1.onsuccess = function() {
          //alert('Must restart phone to start Geckolog !!!');
          var parmArray = new Array();
          parmArray.push('enable');
          parmArray.push('1');
          var initPathRequest2 = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
          initPathRequest2.onsuccess = function() {
            var operatorName = navigator.kaiosExtension.readRoValue('ro.operator.name');
            switch (operatorName) {
              case "TMO":
              case "EASTLINK":
              case "VIDEOTRON":
              case "ATT":
              case "SPR":
                win.setTimeout(function(){
                  debug('restart b2g');
                  var parmArray = new Array();
                  parmArray.push('kaiosreboot');
                  kaiosExtension.execCmdLE(parmArray, 1);
                }, 3000);
                break;
              case "TELUS":
                debug('restart b2g');
                var parmArray = new Array();
                parmArray.push('kaiosreboot');
                kaiosExtension.execCmdLE(parmArray, 1);
                break;
             default:
                break;
            }
          }
        }
      }
    } else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      var parmArray = new Array();
      parmArray.push('path');
      parmArray.push('');
      var initPathRequest =
        kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
      initPathRequest.onsuccess = function() {
        var parmArray = new Array();
        parmArray.push('cmd');
        parmArray.push('');
        var initPathRequest1 = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
        initPathRequest1.onsuccess = function() {
          var parmArray = new Array();
          parmArray.push('enable');
          parmArray.push('0');
          var initPathRequest2 = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
          initPathRequest2.onsuccess = function() {
            debug('restart b2g ');
            var parmArray = new Array();
            parmArray.push('kaiosreboot');
            kaiosExtension.execCmdLE(parmArray, 1);
          }
        }
      }
      refeshfiletimes(key);
    }
  }

  function initloginfo(key) {
    var mDevice = getstoragedevice();
    var time = getTimes();
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
      var dir = (mDevice + '/' + key);
      kaiosExtension.createFileLE('DIRECTORY', dir);
      var path = '/data/initlog' + '/' + key + time + '.log';
      kaiosExtension.createFileLE('FILE', path);
      var parmArray = new Array();
      parmArray.push('path');
      parmArray.push(path);
      var initPathRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
      initPathRequest.onsuccess = function() {
        if (kaiosExtension) {
            //add out by elegentin.xie
            setProps('debug.console.enabled', true);
            setProps('wifi.debugging.enabled', true);
            //add out by elegentin.xie
            var parmArray = new Array();
            parmArray.push('enable');
            parmArray.push('1');
            var initRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
            initRequest.onsuccess = function () {
                //alert('The Phone will be Resart');
                win.setTimeout(function () {
                    var parmArray = new Array();
                    parmArray.push('kaiosreboot');
                    kaiosExtension.execCmdLE(parmArray, 1);
                }, 3000);
            };
        }
      };
    }else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      setProps('debug.console.enabled', false);
      setProps('wifi.debugging.enabled', false);
      if (kaiosExtension) {
        var parmArray = new Array();
        parmArray.push('enable');
        parmArray.push('0');
        var initRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
        initRequest.onsuccess = function() {
          var parmArray = new Array();
          parmArray.push('data_kaioslog_upper');
          var initRequest = kaiosExtension.execCmdLE(parmArray, 1);
          initRequest.onsuccess = function() {
            debug("ZB initloginfo : data_kaioslog_upper success");
            var autoCommand = 'copy initlog';
            var parmArray = new Array();
            parmArray.push(autoCommand);
            parmArray.push(mDevice + '/initlog');
            debug("ZB_COPY parmArray" + parmArray[0] + parmArray[1]);
            var initRequest = kaiosExtension.execCmdLE(parmArray, 2);
            initRequest.onsuccess = function() {
              debug("ZB initloginfo : copy initlog success");
              var parmArray = new Array();
              parmArray.push('delete_kaioslog_file');
              parmArray.push('data/initlog');
              kaiosExtension.execCmdLE(parmArray, 2);
            };
          };
        };
      }
    }
  }

  function jrdlogKeyinfo(key) {
    var mDevice = getstoragedevice();
    var time = getTimes();
    var strPersist = 'persist.sys.kaios' + key + '.enable';
    if ($(key + 'Run').innerHTML === 'Run') {
      $('loading').style.display = 'block';
      $(key + 'Run').innerHTML = 'Stop';
      var path;
      if(key === 'qxdm'){
        path = mDevice + '/' + key;
        kaiosExtension.createFileLE('DIRECTORY', path);
      } else {
        path = mDevice + '/' + key + '/' + key + time + '.log';
        kaiosExtension.createFileLE('FILE', path);
      }
      var parmArray = new Array();
      parmArray.push('path');
      parmArray.push(path);
      var initPathRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
      initPathRequest.onsuccess = function() {
        if (kaiosExtension) {
          var parmArray = new Array();
          parmArray.push('enable');
          parmArray.push('1');
          var initRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
          initRequest.onsuccess = function() {
            debug('start kaioslogKeyinfo');
          };
        }
      };
    }else {
      $('loading').style.display = 'none';
      $(key + 'Run').innerHTML = 'Run';
      setProps('debug.console.enabled', false);
      setProps('wifi.debugging.enabled', false);
      if (kaiosExtension) {
        var parmArray = new Array();
        parmArray.push('enable');
        parmArray.push('0');
        var initRequest = kaiosExtension.setKAIOSLogPara(key, parmArray, 2);
        initRequest.onsuccess = function() {
          debug('stop kaioslogKeyinfo');
        };
      }
      refeshfiletimes(key);
    }
  }

  //get Versioninfo
  function getstoragedevice() {
    var mPosition = getPosition();
    if (mPosition === 'INTERNAL_SDCARD') {
      return '/storage/emulated/kaioslog';
    }else if (mPosition === 'EXTERNAL_SDCARD') {
      return '/storage/sdcard/kaioslog';
    }
  }

  var Versioninfo = function(key) {
    function getVersionInfo() {
      var mResult = '';
      var mDevice = getstoragedevice();
      var time = getTimes();
      var path = mDevice + '/' + key + '/' + key + time + '.log';
      kaiosExtension.createFileLE('FILE', path);

      var mVersionInfo = {'info': [
        {'key': 'System: ', 'value': '/system/system.ver'},
        {'key': 'Boot: ', 'value': '/boot.ver'},
        {'key': 'Data: ', 'value': '/data/userdata.ver'},
        {'key': 'Recovery: ', 'value': '/data/recovery.ver'},
        {'key': 'Modem: ', 'value': '/data/modem.ver'},
        {'key': 'SBL1: ', 'value': '/proc/modem_sbl'},
        {'key': 'TZ: ', 'value': '/proc/modem_tz'},
        {'key': 'RPM: ', 'value': '/proc/modem_rpm'},
        {'key': 'Cmdline: ', 'value': '/proc/cmdline'},
        {'key': 'Secro: ', 'value': '/proc/secro'},
        {'key': 'Study: ', 'value': '/data/study.ver'},
        {'key': 'Custpack: ', 'value': '/custpack/custpack.ver'},
        {'key': 'Tuning: ', 'value': '/proc/tuning'}
      ]};

      if (kaiosExtension) {
        var i;
        mResult = 'Version info:\n';
        var mVInfo = mVersionInfo.info;
        for (i = 0; i < mVInfo.length - 1; i++) {
          var value = kaiosExtension.fileReadLE(mVInfo[i].key);
          if (0 < value.length) {
            if ('\n' == value.substr(value.length - 1)) {
              mResult += mVInfo[i].key + value;
            }
            else {
              mResult += mVInfo[i].key + value + '\n';
            }
          }
        }
        debug('version info = ' + mResult);
        kaiosExtension.fileWriteLE(mResult, path, 'f');
        refeshfiletimes(key);
      }
    }
    getVersionInfo();
  };

  //get SettingsApn
  function SettingsAPN(key) {
    function getSettingsAPN() {
      var mDevice = getstoragedevice();
      var time = getTimes();
      var path = mDevice + '/' + key + '/' + key + time + '.log';
      kaiosExtension.createFileLE('FILE', path);
      var _settings = window.navigator.mozSettings;
      var mKey = 'ril.data.apnSettings';
      var req = _settings.createLock().get(mKey);
      req.onerror = req.onsuccess = function() {
        var value = req.result[mKey];
        var s = getobjstrItemsStr(value);
        debug(' SettingsAPN = ' + s);
        kaiosExtension.fileWriteLE('getSettingsAPN:\n' + s, path, 'f');
        refeshfiletimes(key);
      };

      function getobjstrItemsStr(o) {
        var r = [], i, j = 0, len;
        if (null == o) {
          return o;
        }
        if (typeof o == 'string') {
          return o;
        }
        if (typeof o == 'object') {
          if (!o.sort) {
            for (i in o) {
              if (typeof o[i] != 'function') {
                if (typeof o[i] != 'object') {
                  r[j++] = '';
                  r[j++] = i;
                  r[j++] = ' = ';
                  r[j++] = getobjstrItemsStr(o[i]);
                }
              }
            }
          }else {
            r[j++] = '';
            for (i = 0, len = o.length; i < len; ++i) {
              r[j++] = obj2str(o[i]);
              r[j++] = ',';
            }
            //maybe null
            r[len == 0 ? j : j - 1] = '\n';
          }
          return r.join('');
        }
        return o.toString();
      }

      function obj2str(o) {
        var r = [], i, j = 0, len;
        if (null == o) {
          return o;
        }
        if (typeof o == 'string') {
          return o;
        }
        if (typeof o == 'object') {
          if (!o.sort) {
            for (i in o) {
              if (typeof o[i] != 'function') {
                if (typeof o[i] != 'object') {
                  r[j++] = '\n';
                  r[j++] = i;
                  r[j++] = ' = ';
                }
                r[j++] = obj2str(o[i]);
              }
            }
          }else {
            r[j++] = '\n';
            for (i = 0, len = o.length; i < len; ++i) {
              r[j++] = obj2str(o[i]);
              r[j++] = ',';
            }
            r[len == 0 ? j : j - 1] = '';
          }
          return r.join('');
        }
        return o.toString();
      }
    }
    getSettingsAPN();
  }

  function NvSettingsProp(key, CHECKLIST, type) {
    debug('NvSettingsProp type = ' + type);
    var strSettingsInfo = 'SettingsInfo: \n';
    var strNvInfo = 'NVInfo: \n';
    var mDevice = getstoragedevice();
    var _settings = window.navigator.mozSettings;
    function getinfo(key, type) {
      debug('getinfo type = ' + type);
      loadJSON(CHECKLIST, function(data) {
        if (data) {
          if ('Prop' === type) {
            var time = getTimes();
            var path = mDevice + '/' + key + '/' + key + time + '.log';
            kaiosExtension.createFileLE('FILE', path);
            //var dataprefs = data.prefs;
            settingsinfo(path, _settings, data, 0);
          }
          if ('NV' === type) {
            var time = getTimes();
            var path = mDevice + '/' + key + '/' + key + time + '.log';
            kaiosExtension.createFileLE('FILE', path);
            nvinfo(path, data, 0);
          }
        }
      });
    }

    function settingsinfo(path, _settings, data, i) {
      //-------Step 1 get Settings value;--------
      debug('settingsinfo data = ' + data);
      var req = _settings.createLock().get('*');
      req.onsuccess = function(e) {
        var result = req.result;
        strSettingsInfo += '\n All of Settings Prop\n';
        for (var attr1 in result) {
          var defaultValue = kaiosExtension.readJson(attr1);
          strSettingsInfo += 'key:           ' + attr1 + '\n';

          strSettingsInfo += 'Default Value: ';
          var resultstr = defaultValue;
          if (typeof resultstr == 'object') {
            for (var attr2 in resultstr) {
              var tmp = resultstr[attr2]
              if (typeof resultstr == 'object'){
                for (var attr3 in tmp) {
                  strSettingsInfo += attr3 + ':';
                  strSettingsInfo += tmp[attr3] + '; ';
                }
              } else if (typeof tmp != 'function') {
                strSettingsInfo += attr2 + ':';
                strSettingsInfo += tmp + '; ';
              }
            }
            strSettingsInfo += '\n';
          } else {
            strSettingsInfo += resultstr + '\n';
          }

          strSettingsInfo += 'True Value:    ';
          var resultstr = result[attr1];
          if (typeof resultstr == 'object') {
            for (var attr2 in resultstr) {
              var tmp = resultstr[attr2]
              if (typeof resultstr == 'object'){
                for (var attr3 in tmp) {
                  strSettingsInfo += attr3 + ':';
                  strSettingsInfo += tmp[attr3] + '; ';
                }
              } else if (typeof tmp != 'function') {
                strSettingsInfo += attr2 + ':';
                strSettingsInfo += tmp + '; ';
              }
            }
            strSettingsInfo += '\n';
          } else {
            strSettingsInfo += resultstr + '\n';
          }
        }
        strSettingsInfo += '\n Custmization of Settings Prop\n';
        var datasettings = data.settings;
        for (var attr in datasettings) {
          var keystr = datasettings[attr].key;
          strSettingsInfo += 'Key = ' + keystr + '\n';
          strSettingsInfo += 'Value = ' + result[keystr] + '\n';
          strSettingsInfo += 'DefValue = ' + datasettings[attr].defvalue + '\n';
          strSettingsInfo += 'description = ' +
            datasettings[attr].description + '\n';
        }
        buildprops(path, data);
      };
    }
    function buildprops(path, data) {
      //-------Step 2 get Props value;--------
      debug('buildprops data = ' + data);
      var databuildprops = data.buildprops;
      strSettingsInfo += 'Props:\n';

      for (var attr in databuildprops) {
        var mKey = databuildprops[attr].key;
        strSettingsInfo += '\n Key = ' + mKey + '\n';
        strSettingsInfo += 'Value = ' + kaiosExtension.readRoValue(mKey);
        strSettingsInfo += 'DefValue = ' + databuildprops[attr].defvalue + '\n';
        strSettingsInfo += 'description = ' +
          databuildprops[attr].description + '\n';
      }
      kaiosExtension.fileWriteLE(strSettingsInfo, path, 'f');
      refeshfiletimes(key);
    }
    function nvinfo(path, nv, i) {
      var mKey = parseInt(nv[i].id);
      var req = kaiosExtension.readNvitemEx(mKey);

      req.onerror = req.onsuccess = function() {
        var nvResultArr = null;
        if (null != req.result) {
          nvResultArr = req.result.join();
        }
        strNvInfo += '\nNV Item = ' + nv[i].id + '\n';
        strNvInfo += 'Path = ' + nv[i].path + '\n';
        strNvInfo += 'Defvalue = ' + nv[i].defvalue + '\n';
        strNvInfo += 'Value = ' + nvResultArr + '\n';

        if (i < nv.length - 1) {
          nvinfo(path, nv, i + 1);
        }else if (i == prop.length - 1) {
          kaiosExtension.fileWriteLE(strNvInfo, path, 'f');
          refeshfiletimes(key);
        }
      };
    }
    getinfo(key, type);
  }

  function clearalllogs(key) {
    var mDevice = getstoragedevice();
    var autoCommand = 'delete_kaioslog_file';

    var parmArray = new Array();
    parmArray.push(autoCommand);
    parmArray.push(mDevice + '/*');

    var request = kaiosExtension.execCmdLE(parmArray, 2);
    request.onsuccess = function(e) {
      $(key + 'MdfTime').style.opacity = 1;
      $(key + 'Run').innerHTML = 'Run';
      $('loading').style.display = 'none';
      if(mDevice === '/storage/emulated/kaioslog'){
          $('clearlogcontent').innerHTML = 'Command:<br>' +
              'Removed the logs in Internal Storage';
      }else{
          $('clearlogcontent').innerHTML = 'Command:<br>' +
              'Removed the logs in SD Card Storage';
      }
    };
    request.onerror = function() {
      debug('error:' + JSON.parse(request.error.name).errorInfo + '\n');
    };
  }

  function setProps(key, value) {
    var _settings = window.navigator.mozSettings;
    var cset = {};
    cset[key] = value;
    _settings.createLock().set(cset);
  }
  init();
  addfocuslist();
  fireEvent();

})(window);
