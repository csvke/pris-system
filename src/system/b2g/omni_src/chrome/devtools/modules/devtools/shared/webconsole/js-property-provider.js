"use strict";const DevToolsUtils=require("devtools/shared/DevToolsUtils");if(!isWorker){loader.lazyImporter(this,"Parser","resource://devtools/shared/Parser.jsm");}


const MAX_AUTOCOMPLETE_ATTEMPTS=exports.MAX_AUTOCOMPLETE_ATTEMPTS=100000;const MAX_AUTOCOMPLETIONS=exports.MAX_AUTOCOMPLETIONS=1500;const STATE_NORMAL=0;const STATE_QUOTE=2;const STATE_DQUOTE=3;const OPEN_BODY="{[(".split("");const CLOSE_BODY="}])".split("");const OPEN_CLOSE_BODY={"{":"}","[":"]","(":")",};function hasArrayIndex(str){return/\[\d+\]$/.test(str);}
function findCompletionBeginning(str){let bodyStack=[];let state=STATE_NORMAL;let start=0;let c;for(let i=0;i<str.length;i++){c=str[i];switch(state){case STATE_NORMAL:if(c=='"'){state=STATE_DQUOTE;}else if(c=="'"){state=STATE_QUOTE;}else if(c==";"){start=i+1;}else if(c==" "){start=i+1;}else if(OPEN_BODY.indexOf(c)!=-1){bodyStack.push({token:c,start:start});start=i+1;}else if(CLOSE_BODY.indexOf(c)!=-1){let last=bodyStack.pop();if(!last||OPEN_CLOSE_BODY[last.token]!=c){return{err:"syntax error"};}
if(c=="}"){start=i+1;}else{start=last.start;}}
break;case STATE_DQUOTE:if(c=="\\"){i++;}else if(c=="\n"){return{err:"unterminated string literal"};}else if(c=='"'){state=STATE_NORMAL;}
break;case STATE_QUOTE:if(c=="\\"){i++;}else if(c=="\n"){return{err:"unterminated string literal"};}else if(c=="'"){state=STATE_NORMAL;}
break;}}
return{state:state,startPos:start};}
function JSPropertyProvider(dbgObject,anEnvironment,inputValue,cursor){if(cursor===undefined){cursor=inputValue.length;}
inputValue=inputValue.substring(0,cursor);
let beginning=findCompletionBeginning(inputValue);if(beginning.err){return null;}

if(beginning.state!=STATE_NORMAL){return null;}
let completionPart=inputValue.substring(beginning.startPos);let lastDot=completionPart.lastIndexOf(".");if(completionPart.trim()==""){return null;}


if(!isWorker&&lastDot>0){let parser=new Parser();parser.logExceptions=false;let syntaxTree=parser.get(completionPart.slice(0,lastDot));let lastTree=syntaxTree.getLastSyntaxTree();let lastBody=lastTree&&lastTree.AST.body[lastTree.AST.body.length-1];if(lastBody){let expression=lastBody.expression;let matchProp=completionPart.slice(lastDot+1);if(expression.type==="ArrayExpression"){return getMatchedProps(Array.prototype,matchProp);}else if(expression.type==="Literal"&&(typeof expression.value==="string")){return getMatchedProps(String.prototype,matchProp);}}}
let properties=completionPart.split(".");let matchProp=properties.pop().trimLeft();let obj=dbgObject;
let env=anEnvironment||obj.asEnvironment();if(properties.length===0){return getMatchedPropsInEnvironment(env,matchProp);}
let firstProp=properties.shift().trim();if(firstProp==="this"){try{obj=env.object;}catch(e){}}else if(hasArrayIndex(firstProp)){obj=getArrayMemberProperty(null,env,firstProp);}else{obj=getVariableInEnvironment(env,firstProp);}
if(!isObjectUsable(obj)){return null;}
 
for(let i=0;i<properties.length;i++){let prop=properties[i].trim();if(!prop){return null;}
if(hasArrayIndex(prop)){
obj=getArrayMemberProperty(obj,null,prop);}else{obj=DevToolsUtils.getProperty(obj,prop);}
if(!isObjectUsable(obj)){return null;}} 
if(typeof obj!="object"){return getMatchedProps(obj,matchProp);}
return getMatchedPropsInDbgObject(obj,matchProp);}
function getArrayMemberProperty(obj,env,prop){let propWithoutIndices=prop.substr(0,prop.indexOf("["));if(env){obj=getVariableInEnvironment(env,propWithoutIndices);}else{obj=DevToolsUtils.getProperty(obj,propWithoutIndices);}
if(!isObjectUsable(obj)){return null;}
let result;let arrayIndicesRegex=/\[[^\]]*\]/g;while((result=arrayIndicesRegex.exec(prop))!==null){let indexWithBrackets=result[0];let indexAsText=indexWithBrackets.substr(1,indexWithBrackets.length-2);let index=parseInt(indexAsText,10);if(isNaN(index)){return null;}
obj=DevToolsUtils.getProperty(obj,index);if(!isObjectUsable(obj)){return null;}}
return obj;}
function isObjectUsable(object){if(object==null){return false;}
if(typeof object=="object"&&object.class=="DeadObject"){return false;}
return true;}
function getVariableInEnvironment(anEnvironment,name){return getExactMatchImpl(anEnvironment,name,DebuggerEnvironmentSupport);}
function getMatchedPropsInEnvironment(anEnvironment,match){return getMatchedPropsImpl(anEnvironment,match,DebuggerEnvironmentSupport);}
function getMatchedPropsInDbgObject(dbgObject,match){return getMatchedPropsImpl(dbgObject,match,DebuggerObjectSupport);}
function getMatchedProps(obj,match){if(typeof obj!="object"){obj=obj.constructor.prototype;}
return getMatchedPropsImpl(obj,match,JSObjectSupport);}
function getMatchedPropsImpl(obj,match,{chainIterator,getProperties}){let matches=new Set();let numProps=0;let iter=chainIterator(obj);for(obj of iter){let props=getProperties(obj);numProps+=props.length;
if(numProps>=MAX_AUTOCOMPLETE_ATTEMPTS||matches.size>=MAX_AUTOCOMPLETIONS){break;}
for(let i=0;i<props.length;i++){let prop=props[i];if(prop.indexOf(match)!=0){continue;}
if(prop.indexOf("-")>-1){continue;}

if(+prop!=+prop){matches.add(prop);}
if(matches.size>=MAX_AUTOCOMPLETIONS){break;}}}
return{matchProp:match,matches:[...matches],};}
function getExactMatchImpl(obj,name,{chainIterator,getProperty}){let iter=chainIterator(obj);for(obj of iter){let prop=getProperty(obj,name,obj);if(prop){return prop.value;}}
return undefined;}
var JSObjectSupport={chainIterator:function*(obj){while(obj){yield obj;obj=Object.getPrototypeOf(obj);}},getProperties:function(obj){return Object.getOwnPropertyNames(obj);},getProperty:function(){throw new Error("Unimplemented!");},};var DebuggerObjectSupport={chainIterator:function*(obj){while(obj){yield obj;obj=obj.proto;}},getProperties:function(obj){return obj.getOwnPropertyNames();},getProperty:function(obj,name,rootObj){throw new Error("Unimplemented!");},};var DebuggerEnvironmentSupport={chainIterator:function*(obj){while(obj){yield obj;obj=obj.parent;}},getProperties:function(obj){let names=obj.names();for(let i=0;i<names.length;i++){if(i===names.length-1||names[i+1]>"this"){names.splice(i+1,0,"this");break;}}
return names;},getProperty:function(obj,name){let result;
 try{result=obj.getVariable(name);}catch(e){}
if(result===undefined||result.optimizedOut||result.missingArguments){return null;}
return{value:result};},};exports.JSPropertyProvider=DevToolsUtils.makeInfallible(JSPropertyProvider);exports.FallibleJSPropertyProvider=JSPropertyProvider;